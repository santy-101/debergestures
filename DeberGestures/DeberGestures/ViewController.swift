//
//  ViewController.swift
//  DeberGestures
//
//  Created by Santiago Lema on 29/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var colorView: UIView!
    
    @IBOutlet weak var toquesText: UILabel!
    
    @IBAction func touchView(_ sender: UITapGestureRecognizer) {
    }
    
    
    @IBOutlet weak var numberLabel: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
  
    
        toquesText.text = "\(touch!.tapCount)"
        
        if(Int(toquesText.text!) == Int(numberLabel.text!))
        {
            colorView.backgroundColor = UIColor.blue
        }
    
}
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        colorView.backgroundColor = UIColor.black
        toquesText.text = "0"
        
    }
}
